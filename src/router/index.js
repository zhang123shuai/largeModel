import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import quesAnswers from '../views/question/quesAnswers.vue'
import knowledgeRetrieval from '../views/knowledge/knowledgeRetrieval.vue'
import intelligentBusiness from '../views/intelligent/intelligentBusiness.vue'
import knowledgeBase from '../views/knowledgeBase/knowledgeBase.vue'
import appStore from '../views/appStore/appStore.vue'
import recognitionOCR from '../views/gongneng/recognitionOCR.vue'

const routerPush = VueRouter.prototype.push;
VueRouter.prototype.push = function (location) {
  return routerPush.call(this, location).catch(err => { })
}

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home,
    redirect: '/quesAnswers',
    // meta: {
    //   title: '关于'
    // }
    children: [
      {
        path: '/quesAnswers',
        component: quesAnswers,
        name: 'quesAnswers',
      },
      {
        path: '/knowledgeRetrieval',
        component: knowledgeRetrieval,
        name: 'knowledgeRetrieval',
      },
      {
        path: '/appStore',
        component: appStore,
        name: 'appStore',
      },
      {
        path: '/intelligentBusiness',
        component: intelligentBusiness,
        name: 'appStore',
      },
      {
        path: '/knowledgeBase',
        component: knowledgeBase,
        name: 'knowledgeBase',
      },
      {
        path: '/recognitionOCR',
        component: recognitionOCR,
        name: 'knowledgeBase',
      },
    ],
  },
  // {
  //   path: '/search',
  //   component: search,
  //   name: 'search',
  // },
]
const router = new VueRouter({
  routes
})

router.beforeEach((to, form, next) => {
  if (to.meta.title) {
    document.title = to.meta.title
  } else {
    document.title = '大模型' //此处写默认的title
  }
  next()
})

export default router

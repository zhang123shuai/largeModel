import axios from '@/lib/axios'
import qs from 'qs'

export function ApiDataResourcePhonemohu(data) {
  return axios({
    method: 'post',
    url: `/test/daas/api/daasDMS/ApiDataResource/phonemohu`,
    data
  });
}

export function getAnswersName(data) {
  return axios({
    method: 'post',
    url: `${apiConfig}/QuestionsAnswersController/getAnswersName`,
    data
  });
}

export function getAnswersNameZnyw(data) {
  return axios({
    method: 'post',
    url: `${apiConfig}/IntelligentAnswering/getAnswersName`,
    data
  });
}

export function getAllRecords(data) {
  return axios({
    method: 'post',
    url: `${apiConfig}/QuestionsAnswersController/getAllRecords`,
    data
  });
}

export function getAllRecordsZnyw(data) {
  return axios({
    method: 'post',
    url: `${apiConfig}/IntelligentAnswering/getAllRecords`,
    data
  });
}

export function postAnswer(data) {
  return axios({
    method: 'post',
    url: `${apiConfig}/QuestionsAnswersController/answers?comment=${data.comment}&files=${data.files}&` + `
      temporaryPath=${data.temporaryPath}&path=${data.path}&answersId=${data.answersId}&answersName=${data.answersName}`,
    data
  });
}
export function postAnswerZnyw(data) {
  return axios({
    method: 'post',
    url: `${apiConfig}/IntelligentAnswering/answers?comment=${data.comment}&files=${data.files}&` + `
      temporaryPath=${data.temporaryPath}&path=${data.path}&answersId=${data.answersId}&answersName=${data.answersName}`,
    data
  });
}

export function getFiles(data) {
  return axios({
    method: 'post',
    url: `${apiConfig}/QuestionsController/getFiles`,
    data
  });
}

export function retrieval(data) {
  return axios({
    method: 'post',
    url: `${apiConfig}/KnowledgeRetrieval/retrieval`,
    data
  });
}

export function selectFiled(data) {
  return axios({
    method: 'post',
    url: `${apiConfig}/QuestionsController/selectFiled`,
    data
  });
}

export function download(data) {
  return axios({
    method: 'post',
    url: `${apiConfig}/QuestionsAnswersController/download`,
    data
  });
}

export function deleteFile(data) {
  return axios({
    method: 'post',
    url: `${apiConfig}/QuestionsController/deleteFile`,
    data
  });
}

export function deleteIds(data) {
  return axios({
    method: 'post',
    url: `${apiConfig}/QuestionsAnswersController/deleteIds`,
    data
  });
}

export function deleteIdsZnyw(data) {
  return axios({
    method: 'post',
    url: `${apiConfig}/IntelligentAnswering/deleteIds`,
    data
  });
}

export function getName(data) {
  return axios({
    method: 'post',
    url: `${apiConfig}/QuestionsAnswersController/getName`,
    data
  });
}

export function insert(data) {
  return axios({
    method: 'post',
    url: `${apiConfig}/QuestionsController/insert`,
    data
  });
}

export function uploading(data) {
  return axios({
    method: 'post',
    url: `${apiConfig}/QuestionsController/uploading`,
    data
  });
}

export function deleteFiles(data) {
  return axios({
    method: 'post',
    url: `${apiConfig}/QuestionsController/deleteFiles`,
    data
  });
}

export function manualModification(data) {
  return axios({
    method: 'post',
    url: `${apiConfig}/IntelligentAnswering/manualModification`,
    data
  });
}

export function ocrControllerUpload(data) {
  return axios({
    method: 'post',
    url: `${apiConfig}/ocrController/upload`,
    data
  });
}

export function ocrControllerLoadPdf(data) {
  return axios({
    method: 'post',
    url: `${apiConfig}/ocrController/loadPdf`,
    data,
    responseType: 'blob'
  });
}

export function updateContent(data) {
  return axios({
    method: 'post',
    url: `${apiConfig}/QuestionsAnswersController/updateContent`,
    data,
  });
}

export function addEvaluate(data) {
  return axios({
    method: 'post',
    url: `${apiConfig}/QuestionsAnswersController/addEvaluate`,
    data,
  });
}

export function updateIds(data) {
  return axios({
    method: 'post',
    url: `${apiConfig}/QuestionsAnswersController/updateIds`,
    data,
  });
}

export function updateIdswxwz(data) {
  return axios({
    method: 'post',
    url: `${apiConfig}/IntelligentAnswering/updateIds`,
    data,
  });
}

export function voiceControllerUpload(data) {
  return axios({
    method: 'post',
    url: `${apiConfig}/voiceController/upload`,
    data,
  });
}
// export function downloadWord(params) {
//   return axios({
//     method: 'get',
//     url: `${apiConfig}/IntelligentAnswering/downloadWord?manualId=${params.manualId}`,
//   });
// }

